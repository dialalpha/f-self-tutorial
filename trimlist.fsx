﻿// remove consecutive duplicates from a list
// e.g: [a; a; a; b; c; c; a; d] -> [a; b; c; a; d]

let trimList aList = 
    let rec loop oldList newList prev =
        match oldList with
        | [] -> newList
        | h::t ->
            if prev = h then loop t newList h
            else loop t (newList @ [h]) h
    match aList with
    | [] | [_] -> aList
    | h::t -> loop aList.Tail [aList.Head] aList.Head

// test
//trimList [1,1,1,2,2]
//trimlist [1]
//trimlist ([]:int list)