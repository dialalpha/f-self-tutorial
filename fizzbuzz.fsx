let (|Fizz|Buzz|FizzBuzz|Neither|) n = 
    if n % 15 = 0 then FizzBuzz
    elif n % 5 = 0 then Buzz
    elif n % 3 = 0 then Fizz
    else Neither
    
let classify n = 
    match n with
    | FizzBuzz -> "FizzBuzz"
    | Fizz -> "Fizz"
    | Buzz -> "Buzz"
    | Neither -> string n

// Print FizzBuzz
{1..100} |> Seq.iter (fun x -> printfn "%s" (classify x))