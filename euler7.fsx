﻿// project euler, problem 7

(*
    By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

    What is the 10 001st prime number?
*)

// find divisors of a number
let divisors (max:uint64) = seq {
    for n in 2UL .. (float max |> sqrt |> floor |> uint64) do
        match n with
        | n when (max % n = 0UL) -> yield n
        | _ -> ()    
    }
    
// Check if a number is prime
let isPrime (num:uint64) = divisors num |> Seq.length |> (=) 0

// function that loop till it finds nth prime
let findNthPrime n = 
    let rec loop (num:uint64) n =
        if n = 0 then num-1UL
        else 
            if num |> isPrime then loop (num+1UL) (n-1)
            else loop (num+1UL) n
    loop 2UL n

// solution        
findNthPrime 10001