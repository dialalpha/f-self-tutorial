// project Euler p1
// sum of multiples of 3 and 5 below 1000

let sum1000 =
    [1..999]
    |> List.filter (fun x -> x % 3 = 0 || x % 5 = 0)
    |> List.sum