// Tail recursive fibonacci

let fib x = 
    let rec fibLoop acc1 acc2 x =
        match x with
        | x when x = 0I -> acc1
        | x when x = 1I -> acc2
        | _ -> fibLoop (acc2) (acc1 + acc2) (x - 1I)
    fibLoop 0I 1I x