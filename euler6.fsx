﻿#time "on"

// project Euler, problem 6
(*
    The sum of the squares of the first ten natural numbers is,
        12 + 22 + ... + 102 = 385
    The square of the sum of the first ten natural numbers is,
        (1 + 2 + ... + 10)2 = 552 = 3025
    Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025  385 = 2640.

    Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
*)

// ========= using known properties of these summations ==========
let sumOfSquares n = 
    (2 * n * n * n + 3 * n * n + n) / 6

let squareOfSum n = 
    let sum = (n * n + n) /  2;
    pown sum 2

let euler6Sol = ((sumOfSquares 100) - (squareOfSum 100)) |> abs


// ========== brute force =================
let sumSquares = [|1 .. 100|] |> Array.map (fun i -> i * i) |> Array.sum
let squareSum = [|1 .. 100|] |> Array.sum |> (fun i -> i * i)
let diff = (sumSquares - squareSum) |> abs // solution