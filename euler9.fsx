﻿// project euler, problem 9

(*
    A Pythagorean triplet is a set of three natural numbers, a  b  c, for which,
        a**2 + b**2 = c**2
    For example, 32 + 42 = 9 + 16 = 25 = 52.

    There exists exactly one Pythagorean triplet for which a + b + c = 1000.
    Find the product abc.
*)

let findTriplet max = 
    let rec loop (a, b , c) k =
        if a + b + c = max then ((a,b,c), true)
        elif a + b + c > max then ((a,b,c), false)
        else loop ((k+1)*a, (k+1)*b, (k+1)*c) (k+1)
    loop (3, 4, 5) 1

findTriplet 1000;; // does not work. TBD later