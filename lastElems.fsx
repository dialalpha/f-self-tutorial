// find last element in a list
let rec lastElem alist =
    match alist with
    | [] -> None
    | [n] -> Some(n)
    | h::t -> lastElem t

//test
lastElem ['a'..'z']
//lastElem [8]
//lastElem ([]:int list) 


// find penultimate and ultimate elements
let rec lastTwoElems alist =
    match alist with
    | [] | [_] -> None
    | [m; n] -> Some((m,n))
    | h::t -> lastTwoElems t

lastTwoElems [1 .. 10]