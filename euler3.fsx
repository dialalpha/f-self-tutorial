// Project Euler; problem 3
(*
    The prime factors of 13195 are 5, 7, 13 and 29.
    What is the largest prime factor of the number 600851475143 ?
*)

// step 1: get divisors of input using direct search factorization
//      do trial divisions for 1 upto sqrt max

let divisors (max:uint64) = seq {
    for n in 2UL .. (float max |> sqrt |> floor |> uint64) do
        match n with
        | n when (max % n = 0UL) -> yield n
        | _ -> ()    
    }
    
// step 2: check for prime numbers within the generated divisor sequence and filter
let isPrime (num:uint64) = divisors num |> Seq.length |> (=) 0

// step 3: Solve
let solEuler3 = 
    divisors 600851475143UL
    |> Seq.filter isPrime
    |> Seq.max
