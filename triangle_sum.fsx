﻿// File: triangle_sum.fsx
// Solves Project Euler #18 and #67 and Yodle.com's triangle sum problem
// Author: Alpha Diallo
// Email: adiallo11@citymail.cuny.edu   /   dial.alpha@gmail.com
// Bitbucket: https://bitbucket.org/dialalpha

// To run this script:
//      * fill in the local path to the test file, located in the last line of the script
//      * copy and paste the script into fsi -> magic ensues

open System

/// read text file into jagged array of ints 
let readInput path =
    System.IO.File.ReadAllLines(path)
    |> Array.map (fun str -> str.Split([|' '|], StringSplitOptions.RemoveEmptyEntries))
    |> Array.map (fun arr -> arr |> Array.map (fun elem -> int elem))

/// find max sum given jagged array of ints in triangular form
let maxSum_memoized (triangle:int[][]) =
    let maxDepth = triangle.Length - 1
    let dict = new System.Collections.Generic.Dictionary<int*int, int>()
    let rec loop row col = 
        if row = maxDepth then
            if not (dict.ContainsKey(row,col)) then
                dict.Add((row, col), (triangle.[row].[col]))
        else
            if not (dict.ContainsKey(row, col)) then
                dict.Add((row, col), (triangle.[row].[col] + max (loop (row+1) col) (loop (row+1) (col+1))))
        dict.[(row, col)]
    loop 0 0

/// uncomment last line -> fill in the path -> copy & paste file into fsi -> magic
let solve = readInput >> maxSum_memoized
//solve @"path-to-test-file-here"