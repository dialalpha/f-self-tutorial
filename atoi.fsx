﻿// Author: Alpha Diallo
// dial.alpha@gmail.com
// atoi implementation in F#

open System

/// implements atoi according to C specification. signature: string -> int
let atoi (str : string) =
    let _atoi arr sign =
        arr
        |> Seq.takeWhile (fun e -> Char.IsDigit(e))
        |> Seq.fold (fun acc e -> acc * 10 - 0x30 + int e) 0
        |> (*) sign
    let chars = str.Trim().ToCharArray()
    match chars with
    | [||] -> 0
    | _ ->
        if chars.[0] = '-' then
            chars.[0] <- '0'; _atoi chars -1
        elif chars.[0] = '+' then
            chars.[0] <- '0'; _atoi chars 1
        else _atoi chars 1    