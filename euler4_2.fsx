﻿#time "on"
// project Euler; problem 4
(*
    A palindromic number reads the same both ways. The largest palindrome made from 
    the product of two 2-digit numbers is 9009 = 91 99.

    Find the largest palindrome made from the product of two 3-digit numbers.
*)

// get number of digits in a number
let numDigits num = 
    match num with
    | 0 -> 1
    |_ -> float num |> abs |> log10 |> floor |> int |> (+) 1

// extract single digit from an int
// num: then number
// pos: the position of digit to extract (right to left starting at 1)
// s: the number of digits in the number
let getDigit num pos s = 
    if (pos > s || pos < 1) then -1
    else 
        let rec loop num pos = 
            match pos with
            | 1 -> abs (num % 10)
            | pos when pos > 1 -> loop (num / 10) (pos - 1)
            | _ -> failwith "Mongolian in the system - Position not allowed"
        loop num pos

// check if num is a palindrome
let isNumAPalindrome num = 
    let s = numDigits num
    let rec loop num pos = 
        let front = getDigit num pos s
        let back = getDigit num (s - pos + 1) s
        match pos with
        | pos when pos > (s - pos) -> true
        | _ -> (front = back) && loop num (pos + 1)
    loop num 1

// final step: generate sequence of products, filter palindromes and get max
let seqOf3 = seq{
    for i = 999 downto 900 do
        for j = 999 downto 900 do
            yield i * j
    }

let euler4Sol = seqOf3 |> Seq.filter isNumAPalindrome |> Seq.max
    
