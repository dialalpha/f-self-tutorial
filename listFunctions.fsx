﻿

#time "on"

// implement List.length
let length alist = 
    let rec loop list n =
        match list with
        | [] -> n
        | h::t -> loop t (n+1)
    loop alist 0


// implement List.reverse
let reverse alist = 
    let rec loop oldList newList = 
        match oldList with
        | [] -> newList
        | h::t -> loop t (h::newList)
    loop alist []


// determine if list is a palindrome
let isListAPalindrome aList = 
    List.forall2 (fun elem1 elem2 -> elem1 = elem2) aList (reverse aList) // you can substitute List.rev aList here

#time "off"